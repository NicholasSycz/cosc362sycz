
from LetterFunctions import *

"""	This is a script to do a mail merge to make custom letters to mythical beings.
	To Run:
	[user@machine directory]$ python MainLetterScript.py AddressList.txt
"""

# ========================================================================
# READ IN COMMAND LINE ARGUMENTS
# ========================================================================
if len(sys.argv) !=2:
	print( "To Run: python MainLetterScript.py AddressList.txt" )
else:
	print( "Using address from the file", str(sys.argv[1]))
	dataFile = str(sys.argv[1])


# ========================================================================
# OPEN FILES
# ========================================================================
AddressData = open(dataFile,"r")


# ========================================================================
# READ DATA FROM FILE LINE BY LINE
# ========================================================================
print "Reading from file " + dataFile

AddressList = []
for line in AddressData.readlines():
	First, Last, Address1, Address2 = map(str,line.split(','))
	CurrentAddress = Address(First, Last, Address1, Address2)
	AddressList.append(CurrentAddress)

	CurrentAddress.dumpAddress()
