
import sys
import os
import math
from string import whitespace

"""
	This is my library of functions that can be called in the letter writting main.
"""

def f(x):
	return x*x;


# ================
#  DEFINE A CLASS
# ================
class Address:
	def __init__(self,First,Last,ADLine1,ADLine2):
		self.First   = Last
		self.Last    = First
		self.ADLine1 = ADLine1
		self.ADLine2 = ADLine2

	def dumpAddress(self):
		print "Address Info: "
		print self.First, " ", self.Last, "\n", self.ADLine1, "\n", self.ADLine2

class DocumentText:
	def __init__(self,address):
		self.address = address
		
		self.header	= ("\\documentclass[12pt]{article} \n"
				+ "\\usepackage{geometry} \n"
				+ "\\geometry{hmargin={1in,1in},vmargin={2in,1in}} \n"
				+ "\\begin{document} \n"
				+ "\\thispagestyle{empty} \n\n" )
				
		self.myAddress	= "My Address \n\n\\vskip.5in \n\n"

		self.toAddress	= (self.address.First + " " + self.address.Last + "\n\n"
				+ self.address.ADLine1 +"\n\n"
				+ self.address.ADLine2 +"\n\n")

		self.date	= "\\vskip.5in \n \\today \n\n \\vskip.5in \n\n"

		self.greeting	= "Dear " + self.address.First + " " + self.address.Last + ", \n\n \\vskip.3in"

		self.body	= ("I would like to thank you for being an awesome mythical being and adding so much magic to the world..."
				+ " Even if it's in the form of chaos and destruction."
				+ " Let's watch some anime, my dudes." 
				+ "\n\n \\vskip.3in \n\n \\hskip3.5in Your Friend \n\n"
				+ "\\hskip3.5in DragonSlayer \n\n")

		self.footer	= "\\end{document} \n"

	def WriteLetter(self):
		LetterOutName = str(self.address.Last.lstrip(' ')) + ".tex"
		LetterFile = open(LetterOutName,"w")
		LetterFile.write(self.header)
		LetterFile.write(self.myAddress)
		LetterFile.write(self.toAddress)
		LetterFile.write(self.date)
		LetterFile.write(self.greeting)
		LetterFile.write(self.body)
		LetterFile.write(self.footer)
		LetterFile.close()

