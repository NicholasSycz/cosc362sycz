This assignment explores bash scripting.
I am to write a bash script that will...
	1. recursively look for all LaTeX files
	2. compile these files using the command 'pdflatex'
	3. clean up the .aux, .log, .bib, and other files not being used from the generated .tex file
	4. report out the name and word count statistics of the files compiled into a report in the top directory labeled LatexCompileReport.txt
