#!/bin/bash

filename='./*.tex'
pdfwc='./*.pdf'

echo BEGIN COMPILATION PROCESS FOR ALL LaTeX FILES . . . 
for eachfile in $filename
do
	echo $eachfile
	pdflatex $eachfile
	echo . . .
	echo ' '

	for eachfile in $pdfwc
	do
		for i in $pdfwc
		do
			wc $pdfwc > LatexCompileReport.txt
		done
	done
	echo ' '
done
echo COMPILATION PROCESS COMPLETE.
echo ' '
echo BEGINNING REMOVAL OF UNNECESSARY FILES . . .
rm *.log *.aux *.bib

echo ' '
echo SCRIPT COMPLETE.
echo ' '
echo OH... BTW... Darth Vader came to your house and looked at all your files.
